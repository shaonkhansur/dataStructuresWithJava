package com.company.partOne;

import java.util.ArrayList;
import java.util.Arrays;


public class Array {


    private int[] items;
    private int count;


    public Array(int length) {
        items = new int[length];
    }

    public void insert(int item) {
        //Check if array length is full
        if (items.length == count) {
            //Create a new Array
            int[] newItems = new int[count * 2];
            //Copy all items to newItems
            for (int i = 0; i < count; i++)
                newItems[i] = items[i];
            //Set newItems to items
            items = newItems;
        }
        items[count++] = item;
    }

    public void removeAt(int index) {
        //Validate index
        if (index < 0 || index >= count)
            throw new IllegalArgumentException();

        //Shift left next remaining items
        for (int i = index; i < count; i++ )
            items[i] = items[i + 1];
        count--;
    }

    public int indexOf(int item) {
        for(int i = 0; i < count; i++)
            if (items[i] == item)
                return i;
        return -1;

    }

    public int max() {
        int max = items[0];
        for ( int i = 0; i < count; i++)
            if (items[i] > max)
                max = items[i];
        return max;
    }

    public ArrayList<Integer> intersect(int[] first, int[] second) {
        ArrayList<Integer> container = new ArrayList<>();
        for ( int i = 0; i < first.length; i++)
            container.add(first[i]);
        for ( int i = 0; i < first.length; i++)
            container.add(second[i]);

        return container;

    }

    public void insertAt(int item, int index) {
        //Validate index
        if (index < 0 || index > count)
            throw new IllegalArgumentException();
        int[] newArr = new int[count + 1];
        count++;
        for (int i = 0; i < count; i++)
            newArr[i] = items[i];

        for (int i = count-1; i > index; i--)
            newArr[i] = newArr[i-1];
        newArr[index] = item;
        items = newArr;
    }

    public int length() {
        return count;
    }

    public int[] getItems() {
        return items;
    }



    public void print() {
        System.out.println(Arrays.toString(items));
    }
}
